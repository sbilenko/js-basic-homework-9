/* 
1. За допомогою document.createElement(), insertAdjacentHTML().
Якщо детальніше розібратись, то сюди можна додати і клонування. 

2. Перший параметр означає - куди ми хочемо вставити елемент. 
"beforebegin" – вставити html перед textElement,
"afterbegin" – вставити html на початок textElement,
"beforeend" – вставити html в кінець textElement,
"afterend" – вставити html после textElement.

3. За допомогою remove() 
*/

const cities = [
  'Kiev',
  ['Kharkiv', 'Mariupol', ['Bucha', 'Ivano-Frankivsk', 'Poltava'], 'Sumy'],
  'Odessa',
  'Lviv',
  ['Borispol', 'Irpin'],
]

const parent = document.querySelector('.container')

const showListOnPage = (array, parent) => {
  const list = document.createElement('ul')
  list.classList.add('list')

  array.map((el) => {
    if (typeof el === 'object') {
      const listItem = document.createElement('li')
      listItem.innerHTML = 'Вкладені міста:'
      listItem.classList.add('listItem')

      list.append(listItem)

      showListOnPage(el, listItem)
    } else {
      const listItem = document.createElement('li')
      listItem.classList.add('listItem')
      listItem.innerHTML = `${el}`

      list.append(listItem)
    }
  })

  parent.append(list)
  
  return 
}

showListOnPage(cities, parent)

const timerText = document.createElement('div')
timerText.classList.add('timerText')
timerText.innerHTML = 'Зворотній відлік: '
  
const timerValue = document.createElement('span')
timerValue.classList.add('timerValue')


let sec = 5
let timer
countdown()
function countdown() {
  timerValue.innerHTML = sec
  sec--
  timer = setTimeout(countdown, 1000)
  if (sec <  0) {
    clearTimeout(timer)
    parent.style.display = 'none'
  }
}

timerText.append(timerValue)
parent.append(timerText)